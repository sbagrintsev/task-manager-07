package ru.tsc.bagrintsev.tm.util;

public interface FormatUtil {

    static String bytesFormatString(long bytes) {
        long kilobyte = 1024;
        long megabyte = kilobyte * 1024;
        long gigabyte = megabyte * 1024;
        long terabyte = gigabyte * 1024;
        String result = bytes + " Bytes";
        if ((bytes >= 0) && (bytes < kilobyte)) {
            result = bytes + " B";
        }
        else if ((bytes >= kilobyte) && (bytes < megabyte)) {
            result = (bytes / kilobyte) + " KB";
        }
        else if ((bytes >= megabyte) && (bytes < gigabyte)) {
            result = (bytes / megabyte) + " MB";
        }
        else if ((bytes >= gigabyte) && (bytes < terabyte)) {
            result = (bytes / gigabyte) + " GB";
        }
        else if (bytes >= terabyte) {
            result = (bytes / terabyte) + " TB";
        }
        return result;
    }

}
