package ru.tsc.bagrintsev.tm.model;

import ru.tsc.bagrintsev.tm.constant.CommandLineConst;
import ru.tsc.bagrintsev.tm.constant.RunTimeConst;

public class Command {

    public static final Command HELP = new Command(CommandLineConst.HELP_SHORT, RunTimeConst.HELP, "Print help.");

    public static final Command ABOUT = new Command(CommandLineConst.ABOUT_SHORT, RunTimeConst.ABOUT, "Print about.");

    public static final Command VERSION = new Command(CommandLineConst.VERSION_SHORT, RunTimeConst.VERSION, "Print version.");

    public static final Command INFO = new Command(CommandLineConst.INFO_SHORT, RunTimeConst.INFO, "Print system info.");

    public static final Command EXIT = new Command(CommandLineConst.EMPTY, RunTimeConst.EXIT, "Close program.");

    private String argName;

    private String commandName;

    private String description;

    public Command(String argName, String commandName, String description) {
        this.argName = argName == null? "" : argName;
        this.commandName = commandName == null? "" : commandName;
        this.description = description == null? "" : description;
    }

    public String getArgName() {
        return argName;
    }

    public void setArgName(String argName) {
        this.argName = argName == null? "" : argName;
    }

    public String getCommandName() {
        return commandName;
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName == null? "" : commandName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null? "" : description;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append("[");
        result.append(String.format("%-12s", argName));
        result.append(" | ");
        result.append(String.format("%-13s", commandName));
        result.append("]");
        if (!description.isEmpty()) {
            while (result.length() < 35) {
                result.append(" ");
            }
            result.append(description);
        }

        return result.toString();
    }

}
