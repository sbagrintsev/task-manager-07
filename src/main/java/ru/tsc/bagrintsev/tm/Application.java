package ru.tsc.bagrintsev.tm;

import ru.tsc.bagrintsev.tm.model.Command;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static ru.tsc.bagrintsev.tm.constant.CommandLineConst.*;
import static ru.tsc.bagrintsev.tm.constant.RunTimeConst.*;
import static ru.tsc.bagrintsev.tm.util.FormatUtil.*;

/**
 * @author Sergey Bagrintsev
 * @version 1.7.0
 */

public final class Application {

    public static void main(final String[] args) throws IOException {
        processOnStart(args);

        showWelcome();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            while (true) {
                System.out.println();
                System.out.println("Enter Command:");
                System.out.print(">> ");
                final String command = reader.readLine();
                processOnTheGo(command);
            }
        }
    }

    private static void processOnStart(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case HELP_SHORT:
                showHelp();
                break;
            case VERSION_SHORT:
                showVersion();
                break;
            case ABOUT_SHORT:
                showAbout();
                break;
            case INFO_SHORT:
                showSystemInfo();
                break;
            default:
                showOnStartError(arg);
                showHelp();
        }
    }

    private static void processOnTheGo(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case HELP:
                showHelp();
                break;
            case VERSION:
                showVersion();
                break;
            case ABOUT:
                showAbout();
                break;
            case INFO:
                showSystemInfo();
                break;
            case EXIT:
                close();
                break;
            default:
                showError(command);
                showHelp();
        }
    }

    private static void processOnStart(final String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        processOnStart(arg);
        close();
    }

    private static void showSystemInfo() {
        final Runtime runtime = Runtime.getRuntime();
        final String processors = String.format("Available processors (cores): %d", runtime.availableProcessors());
        final long freeMemoryLong = runtime.freeMemory();
        final String freeMemory = String.format("Free memory: %s", bytesFormatString(freeMemoryLong));
        final long maxMemoryLong = runtime.maxMemory();
        final String maxMemoryStr = maxMemoryLong == Long.MAX_VALUE ? "no limit" : bytesFormatString(maxMemoryLong);
        final String maxMemory = String.format("Maximum memory: %s", maxMemoryStr);
        final long totalMemoryLong = runtime.totalMemory();
        final String totalMemory = String.format("Total memory available to JVM: %s", bytesFormatString(totalMemoryLong));
        final long usedMemoryLong = totalMemoryLong - freeMemoryLong;
        final String usedMemory = String.format("Used memory in JVM: %s", bytesFormatString(usedMemoryLong));
        System.out.printf("%s\n%s\n%s\n%s\n%s\n", processors, maxMemory, totalMemory, freeMemory, usedMemory);
    }

    private static void showWelcome() {
        System.out.println("*** Welcome to Task Manager ***");
    }

    private static void showHelp() {
        System.out.println("[Supported commands]");
        System.out.println("[Command Line | While Running]");
        System.out.println("[-------------|--------------]");
        System.out.println(Command.VERSION);
        System.out.println(Command.HELP);
        System.out.println(Command.ABOUT);
        System.out.println(Command.INFO);
        System.out.println(Command.EXIT);
    }

    private static void showVersion() {
        System.out.println("task-manager version 1.7.0");
    }

    private static void showAbout() {
        System.out.println("[Author]");
        System.out.println("Name: Sergey Bagrintsev");
        System.out.println("E-mail: sbagrintsev@t1-consulting.com");
    }

    private static void close() {
        System.exit(0);
    }

    private static void showError(final String arg) {
        System.err.printf("Error! Command '%s' is not supported...\n", arg);
    }

    private static void showOnStartError(final String arg) {
        System.err.printf("Error! Argument '%s' is not supported...\n", arg);
    }

}
